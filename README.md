# Projet Blog

Créer une plateforme de blog en node/react avec une gestion des utilisateur·ices.

## Fonctionnalités 

Un user doit pouvoir s'inscrire, se connecter et une fois connecter doit pouvoir poster des articles sur son espace personnel.

N'importe qui peut consulter les articles des autres user.


### Optionnelles
Idées de fonctionnalités optionnelles si vous avez le temps et/ou l'envie. (chacune de ces idées rajoute au moins une entité, donc du taff)

* Poster des commentaires sur les articles
* Poster une image dans les articles (pas d'entité pour celle ci) ou des images pour un article (demandera potentiellement une entité)
* Noter les articles


## Réalisation

* Créer un diagramme de Use Case et/ou des user stories pour les différentes fonctionnalités que vous identifierez sur le blog
* Créer une maquette fonctionnelle, au moins mobile, pour 2-3 pages du blog
* Identifier les entités qui persisteront en bdd et faire un diagramme d'entité (diagramme de classe)
* Créer la bdd et un script de mise en place de celle ci
* Créer une API avec node/express
* Créer une interface avec React
* Faire une authentification avec hashing et JWT
